// =========================================//
// SOUL_LootCrate.js
// =========================================//

/*:
* @plugindesc v1.0 Creates a crate type item that contains other items when used / opened. 
* @author Soulpour777 - soulxregalia.wordpress.com
*
* @help This plugin does not contain any plugin commands or parameters.

===========================
SOUL_LootCrate.js
Author: Soulpour777

Date Created: 3:47PM, June 29, 2016

Current Version: 1.0

===========================
Instruction
===========================

In order to use this plugin, you must place this note tag
on your Item notebox:

<lootcrate>

</lootcrate>

It means that whatever is inside the lootcrate note tag,
would be the ones included to be unpacked when the certain
item tagged with it is used. We have three commands:

gainItem(itemId, amount);
gainWeapon(weaponId, amount);
gainArmor(armorId, amount);

where itemId, weaponId, armorId is the id of the specific
item, weapon or armor you want to be unpacked, while the
amount is how many of those items do you want gained.

For example, I want to gain items 1, 2, 3 and 4...
I can say place something like this on my item note box:

<lootcrate>
gainItem(1, 1);
gainItem(2, 1);
gainItem(3, 1);
gainItem(4, 1);
</lootcrate>

Now, when I use the item with this note tag, what it
does is it is going to consume the item you used
and unpack items 1, 2, 3, 4.

Same goes if you want some weapon and armor
instead of items:

<lootcrate>
gainWeapon(8, 1);
gainArmor(10,1);
gainArmor(11,1)
gainArmor(12, 1);
gainArmor(13, 1);
</lootcrate>

For random items, you can do:

<lootcrate>
gainItem(Math.floor(Math.random() * 10), 1);
</lootcrate>

It lets you randomize the value whether you want
iten 1, 2... randomly. The *10 on the formula
is the value of the randomized number. That means
from item 1 to 10.

This works for Item Noteboxes only. This is to
simulate Loot Crates, as seen in real life.

*
*/
var Imported = Imported || {};
Imported.SOUL_LootCrate = true;

var Soulpour777 = Soulpour777 || {};
Soulpour777.LootCrate = Soulpour777.LootCrate || {};
Soulpour777.LootCrate.lootCrateEvalValues = [];

(function($){
	Soulpour777.LootCrate.Game_Battler_useItem = Game_Battler.prototype.useItem;
	$.prototype.useItem = function(item) {
		Soulpour777.LootCrate.Game_Battler_useItem.call(this, item);
	    if (DataManager.isItem(item)) {
	    	// this is the place where I called the shiftNotetag function
	    	// to check if I have the note tag in the item specifically used.
	    	shiftNotetag(item);
	    }
	};

})(Game_Battler);

(function(){
	// this shifts the proper item value and sees if it has the proper note tag,
	// if so, it creates them as eval method.

	// nahirapan ako dito, seriously. nakailang trial and error bago ko nakuha yung
	// tamang regular expression code.
	shiftNotetag = function(item) {
		var regEx = /<\s*lootcrate\s*>([\d\D\n\r]*)<\/\s*lootcrate\s*>/im;
		if (item.note.match(regEx)) {
			eval(RegExp.$1);
		}
	};		
})(); 

	// I removed them as part of a function because somehow they can't be called.
	// trans: pucha ayaw maconvert na eval kapag nasa enclosed function.

	function gainArmor(armorId, amount) {
		$gameParty.gainItem($dataArmors[armorId], amount);
	}

	function gainWeapon(weaponId, amount) {
		$gameParty.gainItem($dataWeapons[weaponId], amount);
	}

	function gainItem(itemId, amount) {
		$gameParty.gainItem($dataItems[itemId], amount);
	}


